﻿# PI4-2022-TeQuePlantei
 
 Projeto relacionado ao Projeto Interdisciplinar do 4º semestre de DSM, FATEC Franca.
 
<h2>Bibliotecas e <i>boards</i> utilizadas no código do Arduino:</h2>

 

[ESP8266 <i>Board</i>](https://github.com/esp8266/Arduino)<br>
[ESP8266 WiFi](https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WiFi)<br>
[Firebase ESP Client](https://github.com/mobizt/Firebase-ESP-Client)<br>
[Cayenne MQTT](https://github.com/myDevicesIoT/Cayenne-MQTT-ESP)<br>
[NTPClient](https://github.com/arduino-libraries/NTPClient)<br>
[WifiUdP](https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266WiFi/src/WiFiUdp.h)<br>
[Adafruit DHT](https://github.com/adafruit/DHT-sensor-library)<br>


[<b>Cayenne Dashboard</b>](https://cayenne.mydevices.com/shared/63545c1e0d1bfa5ea9e360a0), onde apresentamos as informações coletadas
 
